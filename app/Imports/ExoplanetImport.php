<?php

namespace App\Imports;

use App\Models\Exoplanet;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ExoplanetImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Exoplanet([
            'planet_name' => $row[1],
            'host_name' => $row[2],
            'planets' => (int) $row[4],
            'stars' => (int) $row[3],
            'discovery_method' => $row[5],
            'discovery_year' => (int) $row[6],
            'discovery_facility' => $row[7],
            'orbital_period' => $row[8],
            'orbit_semimajor_axis' =>  $row[12],
            'distance' => $row[16],
        ]);
    }
    public function startRow(): int
    {
        return 2;
    }
}
