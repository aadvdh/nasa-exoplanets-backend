<?php

namespace Database\Factories;

use App\Models\Exoplanet;
use Illuminate\Database\Eloquent\Factories\Factory;

class ExoplanetFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Exoplanet::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
