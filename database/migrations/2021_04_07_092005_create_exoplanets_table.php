<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExoplanetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exoplanets', function (Blueprint $table) {
            $table->id();
            $table->string("planet_name");
            $table->string("host_name");
            $table->integer("stars");
            $table->integer("planets");
            $table->string('discovery_method');
            $table->integer("discovery_year");
            $table->string("discovery_facility");
            $table->double('orbital_period')->nullable();
            $table->double('orbit_semimajor_axis')->nullable();
            $table->double('distance')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exoplanets');
    }
}
