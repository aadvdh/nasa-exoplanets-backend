<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exoplanet extends Model
{
    protected $hidden = ['id', 'created_at', 'updated_at'];
    protected $fillable = ['planet_name', 'host_name', 'stars', 'planets', 'discovery_method', 'discovery_year', 'discovery_facility', 'orbital_period', 'orbit_semimajor_axis', 'distance'];
    use HasFactory;
}
