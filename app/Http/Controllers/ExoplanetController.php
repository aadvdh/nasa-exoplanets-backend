<?php

namespace App\Http\Controllers;

use App\Imports\ExoplanetImport;
use App\Models\Exoplanet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\QueryBuilder\QueryBuilder;

class ExoplanetController extends Controller
{

    public function import() {
        Excel::import(new ExoplanetImport, 'exoplanets.csv');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $planets = QueryBuilder::for(Exoplanet::class)
        ->allowedSorts(['discovery_year', 'discovery_method', 'host_name','discovery_facility', 'planet_name', 'stars', 'planets', 'orbital_period','orbit_semimajor_axis', 'distance'])
        ->allowedFilters(['discovery_year', 'discovery_method', 'host_name','discovery_facility'])->paginate(20);
        return $planets;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Exoplanet  $exoplanet
     * @return \Illuminate\Http\Response
     */
    public function show(Exoplanet $exoplanet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Exoplanet  $exoplanet
     * @return \Illuminate\Http\Response
     */
    public function edit(Exoplanet $exoplanet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Exoplanet  $exoplanet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exoplanet $exoplanet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Exoplanet  $exoplanet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exoplanet $exoplanet)
    {
        //
    }

    public function filterable(Request $request) {


        $filterables = Cache::remember('filterable', 3600, function () {
            $columns = ['discovery_facility', 'discovery_year', 'discovery_method'];
            $planets =  Exoplanet::query()->distinct();
            $arr = [];
            foreach($columns as $col) {
                $arr[$col] = $planets->pluck($col)->toArray();

                sort($arr[$col]);
            }
            return $arr;
        });

        return Response()->json($filterables);
    }
}
